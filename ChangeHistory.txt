Changes up until v2.7
---------------------
19.01.2012
- changed to Maven, deployment to maven repository

17.04.2008
- added support for WSW (Warsow) (thanks mcrandello)
- fixed BF2 server query
- added Q4 (Quake 4) support (mcrandello)
- added COD4 query type for convenience (already supported as it is the same as COD2 query)
- added getFullResponse() to ServerInfo to return the actual results from the details query.  This can then be parsed as wanted.

20.1.2006
- added Q4 (Quake 4) support (mcrandello)

14.11.2005
- added support for NEX (Nexuiz) (thanks mcrandello)
- added COD2 (Call of Duty 2) info - uses COD query
- added MOH (Medal of Honor) info - uses UT query
  
28.09.2005
- added support for AA (Americas Army)
- fixed HL player query for

29.06.2005
- BF2 player query receives multiple packets now
- UT2003 and UT2004 support fixed plus code UTK3 and UTK4 added (thanks xkr47)
- added getSupportedGame() method that returns a HashMap of the game codes and names of the games supported

11.06.2005
- Halflife Source (HL2) added
- BF2 support added
- Never Winter Nights server query added (player query not available)
- Halflife (original) query updated
- A couple of minor bugs fixed.

1st Quarter of 2004
- changed the API - Objects are returned with the info in now
- added support for BFV
- added support for UT2003
- added support for UT2004
- added some default port checking and trying to figure out what the server type is

18.11.2003
- increased timeout to 5000ms
- removed unnecessary call to InetAddress.getByAddress

18.03.2003
- added port to output

17.07.2003
- added simple bf query
- added checking for different game types
