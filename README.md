###GameSJQ
The purpose of this project is to handle the work of gathering information from game servers for my project: http://www.projectgxp.com
It is written in Java for being fast and portable, and easy to work with.
The project was created to fill the void of no open source Java game server query library.

###License
The source code is released under MIT License.
Please refer to the included License file.



